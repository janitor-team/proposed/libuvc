Source: libuvc
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 cmake,
 pkg-config,
 libusb-1.0-0-dev,
 libjpeg-dev,
Build-Depends-Indep:
 doxygen,
Standards-Version: 4.1.1
Section: devel
Homepage: https://github.com/ktossell/libuvc
Vcs-Git: https://salsa.debian.org/multimedia-team/libuvc.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/libuvc

Package: libuvc-dev
Section: libdevel
Architecture: any
Depends:
 libuvc0 (= ${binary:Version}),
 ${misc:Depends},
Description: cross-platform library for USB video devices - development files
 libuvc is a cross-platform library for USB video devices, built atop libusb.
 It enables fine-grained control over USB video devices exporting the
 standard USB Video Class (UVC) interface, enabling developers to write
 drivers for previously unsupported devices, or just access UVC devices in a
 generic fashion.
 .
 This package contains the files necessary to compile applications using
 libuvc.

Package: libuvc0
Section: libs
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Description: cross-platform library for USB video devices
 libuvc is a cross-platform library for USB video devices, built atop libusb.
 It enables fine-grained control over USB video devices exporting the
 standard USB Video Class (UVC) interface, enabling developers to write
 drivers for previously unsupported devices, or just access UVC devices in a
 generic fashion.
 .
 This package contains the shared objects necessary to run an application using
 libuvc.

Package: libuvc-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Suggests: libuvc-dev,
Built-Using: ${doxygen:Built-Using}
Description: cross-platform library for USB video devices - documentation
 libuvc is a cross-platform library for USB video devices, built atop libusb.
 It enables fine-grained control over USB video devices exporting the
 standard USB Video Class (UVC) interface, enabling developers to write
 drivers for previously unsupported devices, or just access UVC devices in a
 generic fashion.
 .
 This package contains the html documentation for the libuvc API.
